# scc_outputs



## About
This folder is for linking scc outputs for different codebases as required by the Security Team for Security reviews



## Add your files
Once you have run the `scc` command at the root of the codebase you want reviewed by security, copy that output into a .txt file in this folder and link it to the security team for review
